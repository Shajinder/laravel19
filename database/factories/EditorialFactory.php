<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Editorial;
use App\Place;
use Faker\Generator as Faker;

$factory->define(Editorial::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'author' => $faker->firstname(3),
        'place_id' => Place::all()->random(1)[0]->id
    ];
});
