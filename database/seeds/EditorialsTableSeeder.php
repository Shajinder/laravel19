<?php

use Illuminate\Database\Seeder;
use App\Editorial;

class EditorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Editorial::create([
            'name' => 'SM',
            ]);
        
        Editorial::create([
                'name' => 'Planeta',
                ]);
        
        Editorial::create([
                    'name' => 'Santillana',
                    ]);
        
        Editorial::create([
                        'name' => 'Anaya',
                        ]);
    }
}
