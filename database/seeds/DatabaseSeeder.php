<?php

use Illuminate\Database\Seeder;

//Solo se ejecuta los seeders que estan, en el Database.
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //primero hay crear sitios.
        $this->call(PlacesTableSeeder::class);
        $this->call(EditorialsTableSeeder::class);
        $this->call(CduTableSeeder::class);
        $this->call(BooksTableSeeder::class);

    }
}
