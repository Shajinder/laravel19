<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
    protected $fillable = ['name'];    

    public function Editorial()
    {
        return $this->belongsTo('App\Editorial');
    }
}
