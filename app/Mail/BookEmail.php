<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request;
use App\Book;
use App\Place;
use App\Mail\BookEmail;
use Illuminate\Support\Facades\Mail;

class BookEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $data = array();

    public function __construct($data)
    {
        //Cargamos el pdf
        $this->path = 'listadelibros.pdf';
        $books = Book::All();
        $this->pdf = \PDF::loadView('pdf',['books' => $books ])
        ->save($this->path);

        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("shajinder@gmail.com")
            ->subject($this->data['subject'])
            ->attach($this->path, [
            'as' => 'listadelibros.pdf', 
            'mime' => 'application/pdf',
            ])
            ->markdown('emails.books',['message' => $this->data['message']]);
    }
}
