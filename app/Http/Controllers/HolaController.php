<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HolaController extends Controller
{
    public function holamundo(){

        $saludo = '¿Qué tal va?';
        $greetings = array('Hell','Bonjour','Bon Sera');
        return view('hola',[
            'saludo' => $saludo,
            'greetings' => $greetings]);

        //hola.blade.php
        //hola.php
        //hola.html

    }
}
