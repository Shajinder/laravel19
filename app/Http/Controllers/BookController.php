<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Place;
use Illuminate\Support\Facades\Mail;
use App\Mail\BookEmail;


class BookController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny',Book::class);
        $books = Book::paginate();
        return view('book.index', ['books' => $books ]);
    }

    public function show($id,Request $request)
    {
        //$user = \Auth::user();
        $book = Book::find($id);
        
        /*if(!$user->can('view',$book)){
            return 'alto';
        }*/

        $this->authorize('view',$book);
        //guaradmos libro como último visitado.
        $request->session()->put('lastbook',$book);
        $books = $request->session()->get('books');
        if(!$books){
            $books = array();
        }
            $books[] = $book;
            $request->session()->put('books',$books);
        //$book = Book::findFail($id);
        return view('book.show',['book' => $book ]);
    }

    public function create()
    {
        $places = Place::All();
        return view('book.create',['places' => $places ]);
    }

    public function store(Request $request)
    {
        
        //como leer los parametros.
        
        //metodo 1

        // $request->title;
        // $book = new Book;
        // $book ->title = $request->title;
        // $book->save();
        
        //metodo 2
        //$book = Book::create(['title' => $request->title]);

        //metodo 3
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'place_id' => 'required|exists:places,id',
            'author' => 'required|max:255',
        ]);
        Book::create($request->all());

        return redirect('/books');
    }
    
    public function forget(Request $request){

        $request->session()->forget('lastbook');
        $request->session()->forget('books');
        return back();
    }
    public function destroy($id)
    {
        
        //dd($id);
        $book = Book::find($id);
        $this->authorize('delete',$book);
        Book::destroy($id);
        return back();
        //$book->delete();
        //return redirect('/books);
    }


    public function edit($id)
    {
        $book = Book::find($id);
        $places = Place::All();
        return view('book.edit',['book' => $book,'places' => $places ]);

    }

    public function update($id,Request $request)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->author = $request->author;
        $book->save();

        return redirect('/books/' . $book->id );
    }

    //imprimir pdf
    function imprimir(){

        $this->authorize('viewAny',Book::class);
        $books = Book::All();
        $pdf = \PDF::loadView('pdf',['books' => $books ]);

        return $pdf->download('listaLibros.pdf');
    }

    function rellenarEmail(){
        $this->authorize('viewAny',Book::class);
        return view('book.mail');
    }

    function enviarEmail(Request $request){

        $data = array(
            'message' => $request->description,
            'subject' => $request->subject
        );

        Mail::to($request->to)->send(new BookEmail($data));

        return back()->with('success','El email se ha enviado corectamente.');
    }



}
