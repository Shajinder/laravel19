<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //fillable cuando usa el create, en  el book controller.
    protected $fillable = ['title','author','editorial_id','cdu'];    

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

    public function editorial()
    {
        return $this->belongsto('App\Editorial');
    }

    public function CDU()
    {
        return $this->belongsto('App\Cdu','cdu','cdu');
    }
    


}


