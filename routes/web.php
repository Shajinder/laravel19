<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Mail\BookEmail;
use Illuminate\Support\Facades\Mail;
Route::get('/', function () {
    return view('welcome');
});



Route::get('/hello/{name}',function($name){
    return "Hola $name";
});

Route::get('/hola','HolaController@holamundo');

//Route::get('books','BookController@index');
//Route::get('books/create','BookController@create');
//Route::get('books/{id}','BookController@show');
//Route::post('books','BookController@store');
//Route::get('books/{id}',BookController@edit)
//Route::get('books/{id}',BookController@update)
//Route::get('books/{id}',BookController@destroy)

//son equilaventas a las rutas de arriba
Route::get('books/email','BookController@rellenarEmail');
Route::post('books/sendemail','BookController@enviarEmail');
Route::get('books/forget','BookController@forget');
Route::resource('books','BookController');
Route::resource('places','PlaceController');
Route::get('places/{id}/json','PlaceController@showJSON');
Route::resource('editorials','EditorialController');
Auth::routes();
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');
Route::get('/redirect/github', 'SocialAuthGoogleController@redirectGithub');
Route::get('/github/callback', 'SocialAuthGoogleController@callbackGithub');
Route::get('/home', 'HomeController@index')->name('home');
Route::name('imprimir')->get('/imprimir-pdf','BookController@imprimir');
Route::get('/email', function () {

    Mail::to("bhupinder7727@gmail.com")->send(new BookEmail());
    return new BookEmail();
});

