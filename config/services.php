<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '420305383788-uc505fd3hakj1uju7tq7d62j90i8s9sr.apps.googleusercontent.com', 
        'client_secret' => 'zmGEcbUZN6b2r3dfdv-JUo0H', 
        'redirect' => 'http://laravel1.com/callback'
    ],

    'github' => [
        'client_id' => 'e07b4ea3f3c8181b4ca9', 
        'client_secret' => '486cce786bf756989219552a7ed3bc0d52c3ec1a', 
        'redirect' => 'http://laravel1.com/github/callback'
    ],

];
