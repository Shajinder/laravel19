<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Lista de libros</h1>
    <table>
        <tr>
            <th>Título</th>
            <th>Autor</th>
            <th>Place</th>
        </tr>
    @forelse($books as $book)
        <tr>
            <td>{{ $book->title }}</td>
            <td>{{ $book->author }}</td>
            <td>{{ $book->place->name }}</td>
        </tr>
    @empty<tr><td><i>No hay libros</i></td></tr>
    @endforelse
    </table>

</body>
</html>
