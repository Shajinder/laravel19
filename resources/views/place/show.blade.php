<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Detales de los Sitios</h1>
    <ul>
        <li>Nombre:{{ $place->name }}</li>
        Lista de llibros:
        @forelse($place->books as $book )
            <li>{{ $book->title  }}</li>
        @empty<li>El sitio no tiene ningún libro.</li>
        @endforelse
    </ul>
</body>
</html>