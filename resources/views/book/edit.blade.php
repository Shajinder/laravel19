<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Edición del libro</h1>
    <form action="/books/{{ $book->id }}" method="post">
    @csrf
        Título:
        <input type="text" name="title" value="{{ $book->title }}"/><br/>
        Autor:
        <input type="text" name="author" value="{{ $book->author }}"/><br/>
        Sitios:
        <select>
        @foreach ($places as $place)
            @if($book->place->id == $place->id )
                <option selected value="{{ $place->id }}">{{ $place->name }}</option>
            @else
            <option value="{{ $place->id }}">{{ $place->name }}</option>
            @endif
        @endforeach
        </select>
        <input type="hidden" name="_method" value="put"><!--Metodo put.-->
        <input type="submit" value="editar"/>
    </form>
</body>
</html>