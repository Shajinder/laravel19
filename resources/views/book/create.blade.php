<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Alta de libro</h1>
    <form action="/books" method="post">
    @csrf
        Título:
        <input type="text" name="title" value="{{ old('title')}}"/>
        {{ $errors->first('title') }}
        <br/>
        Autor:
        <input type="text" name="author" value="{{ old('author')}}"/>
        {{ $errors->first('author') }}
        <br/>
        Sitio:
        <select name="place_id">
        @foreach ($places as $place)
            <option value="{{ $place->id }}" {{ $place->id == old('place_id' ? 'selected':'')}}>{{ $place->name }}</option>
        @endforeach
        </select>
        {{ $errors->first('place_id') }}
        <input type="submit" value="Crear"/>
    </form>
    {{ dd(Session::all()) }}
</body>
</html>