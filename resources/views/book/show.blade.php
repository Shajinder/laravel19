<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Detales del llibro{{ $book ->id }}</h1>
    <ul>
        <li>Título:{{ $book->title }}</li>
        <li>Autor: {{ $book->author }}</li>
        <li>Sitio: {{ $book->place->name }}</li>
    </ul>
</body>
</html>