@extends('layouts.app')

@section('content')
    <h1>Lista de libros</h1>
    El último libro es:
    @if( Session::get('lastbook'))
        {{ Session::get('lastbook')->title }}
        <a href="/books/forget">Olvidar del último libro.</a>
    @else
        Ninguno
    @endif
    <hr>
    Los libros visitados son:
    @if (Session::get('books'))

        @foreach( Session::get('books') as $book)
        <li>{{ $book->title }}</li>
        @endforeach
    @else
        <li>No has visitado ningún libro.</li>
    @endif
<table>
    <tr>
        <th>Título</th>
        <th>Autor</th>
        <th>Place</th>
        <th><a class="btn btn-primary" href="/books/create">Crear</a></th>
        <th><a class="btn btn-primary" href="{{route('imprimir')}}">Ver en pdf</a></th>
        <th><a class="btn btn-primary" href="/books/email">Enviar Email</a></th>
    </tr>
@forelse($books as $book)
    <tr>
        <td>{{ $book->title }}</td>
        <td>{{ $book->author }}</td>
        <td>{{ $book->place->name }}</td>
        @can('view',$book)
        <td><a class="btn btn-primary" href="/books/{{ $book->id }}">Ver</td>
        @endcan
        <td><a class="btn btn-primary" href="/books/{{ $book->id }}/edit">Editar</td>
        <td><form action="/books/{{ $book->id }}" method="post">

        @csrf
            @can('delete',$book)
            <input type="hidden" name="_method" value="delete"/><br/>
            <input class="btn btn-primary"  type="submit" value="borrar"/>
            @endcan
        </form></td>
    </tr>
@empty<tr><td><i>No hay libros</i></td></tr>
@endforelse
</table>
{{ $books->render() }}
@endsection