@extends('layouts.app')

@section('content')
    <div class="container">
<h1>Enviar Correo</h1>

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
        @endif

        <form action="/books/sendemail" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">To</label>
            <input type="email" name="to" class="form-control">
            <label for="exampleFormControlInput1">Subject</label>
            <input type="text" name="subject" class="form-control">
            <label for="exampleFormControlInput1">Description</label>
            <textarea name ="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
            <p>Con el correo se mandará adjunto el pdf con lista de libros.</p>
            <input type="submit" value="Enviar"/>
            
        </form>
    </div>
@endsection