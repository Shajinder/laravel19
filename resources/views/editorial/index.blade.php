@extends('layouts.app')

@section('content')
    <h1>Lista de Editoriales</h1>
<table>
    <tr>
        <th>Nombre</th>
    </tr>
    <tr>
        <td><a class="btn btn-primary" href="/editorials/create">Crear</a></td>
    </tr>
@forelse($editorials as $editorial)
    <tr>
        <td>{{ $editorial->name }}</td>
        <td><a class="btn btn-primary" href="/editorials/{{ $editorial->id }}">Ver</td>
        <td><a class="btn btn-primary" href="/editorials/{{ $editorial->id }}/edit">Editar</td>
        <td><form action="/editorials/{{ $editorial->id }}" method="post">
        @csrf
            <input type="hidden" name="_method" value="delete"/><br/>
            <input class="btn btn-primary"  type="submit" value="borrar"/>
        </form></td>
    </tr>
@empty<tr><td><i>No hay editoriales.</i></td></tr>
@endforelse
</table>
@endsection;